const app = require('express')();
const httpServer = require('http').createServer(app);
const io = require('socket.io')(httpServer, {
  cors: {origin : '*'}
});

const port = process.env.PORT || 3000;

const gameId = 1;
const playerId = 1;
const message = "";
const chatId = 1;

const chat = {
  gameId,
  playerId,
  message,
  chatId
}

const chatMessages = [];

io.on('connection', (socket) => {
  console.log('a user connected');

  socket.on('message', (message) => {
    chatMessages.push(message);
    console.log(message);
    io.emit("message",message);
  });

  socket.on('disconnect', () => {
    console.log('a user disconnected!');
  });
});

httpServer.listen(port, () => console.log(`listening on port ${port}`));
