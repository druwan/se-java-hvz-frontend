const express = require('express');
const path = require('path');
const app = express();
// Use as working directory
app.use(express.static(__dirname + '/dist/se-java-hvz-frontend'));
// Serve files to endpoint /*
app.get('/*', function(req,res) {
  res.sendFile(path.join(__dirname+
    '/dist/se-java-hvz-frontend/index.html'));});
// Listen to for PORT or set port to 8080
app.listen(process.env.PORT || 8080);
