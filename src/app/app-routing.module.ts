import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CreateGameComponent} from './views/create-game/create-game.component';
import {GameDetailComponent} from "./views/game-detail/game-detail.component";
import {LandingPageComponent} from "./views/landing-page/landing-page.component";
import {AuthGuard} from "./services/auth/auth.guard";

const routes: Routes = [
  {path: "game/:id", component: GameDetailComponent, canActivate: [AuthGuard]},
  {path: 'game', component: GameDetailComponent, canActivate: [AuthGuard]},
  {path: 'newgame', component: CreateGameComponent, canActivate: [AuthGuard]},
  {path: '', component: LandingPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
