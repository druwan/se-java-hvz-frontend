import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { QRCodeModule } from 'angular2-qrcode';

import { ChatComponent } from './components/game/game-chat/chat.component';
import { FormsModule } from '@angular/forms';
import { LandingPageComponent } from './views/landing-page/landing-page.component';
import { GameDetailComponent } from './views/game-detail/game-detail.component';
import { CreateGameComponent } from './views/create-game/create-game.component';
import { KillFormComponent } from './components/game/kill-form/kill-form.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { GameListComponent } from './components/game/game-list/game-list.component';
import { GameMapComponent } from './components/game/game-map/game-map.component';
import { PlayerListComponent } from './components/game/player-list/player-list.component';
import { GameTableComponent } from './components/game/game-table/game-table.component';
import { GameAlertComponent } from './components/game/game-alert/game-alert.component';
import { environment } from '../environments/environment';

function initializeKeycloak(keycloak: KeycloakService) {
  return () =>
    keycloak.init({
      config: {
        url: environment.AUTHENTICATION_URL,
        realm: 'master',
        clientId: 'client-id',
      },
      initOptions: {
        onLoad: 'check-sso',
        silentCheckSsoRedirectUri:
          window.location.origin + '/assets/silent-check-sso.html',
      },
    });
}

@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    LandingPageComponent,
    GameDetailComponent,
    CreateGameComponent,
    KillFormComponent,
    NavbarComponent,
    GameListComponent,
    GameMapComponent,
    PlayerListComponent,
    GameTableComponent,
    GameAlertComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    KeycloakAngularModule,
    QRCodeModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
