import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-game-alert',
  templateUrl: './game-alert.component.html',
  styleUrls: ['./game-alert.component.css'],
})
export class GameAlertComponent implements OnInit {
  @Input() type = 'primary';
  @Input() message = 'message';
  /**
   * Duration of alert
   */
  @Input() millis = 1000;
  showAlert = true;

  constructor() {}

  ngOnInit(): void {
    setTimeout(() => {
      this.showAlert = false;
    }, this.millis);
  }
}
