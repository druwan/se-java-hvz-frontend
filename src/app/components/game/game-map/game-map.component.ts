import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Observable, Subscriber } from 'rxjs';
import * as L from 'leaflet';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-game-map',
  templateUrl: './game-map.component.html',
  styleUrls: ['./game-map.component.css'],
})
export class GameMapComponent implements AfterViewInit, OnChanges {
  map: any;
  myMarkers: any;
  circleMarker: any;
  marker: any;
  @Output() coordinateChanged: EventEmitter<object> = new EventEmitter();

  // Default value for map radius
  @Input() mapRadius: number = 100;

  constructor() {}

  ngAfterViewInit(): void {
    this.loadMap();
  }

  getNewCoords(): any {
    // create layer for markers
    this.myMarkers = L.layerGroup().addTo(this.map);
    return this.map.on('click', <LeafletMouseEvent>(e: any) => {
      const latChosen: number = e.latlng.lat;
      const longChosen: number = e.latlng.lng;
      const newCoord: object = {
        Latitude: latChosen,
        Longitude: longChosen,
      };
      this.myMarkers.clearLayers();
      // Create Marker on click-coord
      this.marker = L.marker([latChosen, longChosen])
        .bindPopup(
          `Game location <br> Latitude: ${latChosen} <br> Longitude: ${longChosen}`
        )
        .addTo(this.myMarkers);
      this.coordinateChanged.emit(newCoord);

      // Add a gameZone on click, could be a slider for size, and rectangle, but thats for the future
      this.circleMarker = L.circle([latChosen, longChosen], {
        color: '#B30808',
        fillOpacity: 0.55,
        radius: this.mapRadius,
      }).addTo(this.myMarkers);
    });
  }

  loadMap(): void {
    // Create start map, Europe centered
    this.map = L.map('map').setView([54, 15], 2.5);
    L.tileLayer(
      'https://api.maptiler.com/maps/toner/{z}/{x}/{y}.png?key={accessToken}',
      {
        attribution:
          '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>',
        accessToken: environment.MAPTILER_KEY,
      }
    ).addTo(this.map);

    // if browser have location enabled fly there
    if ('geolocation' in navigator) {
      this.getCurrentPosition().subscribe((position: any) => {
        this.map.flyTo([position.latitude, position.longitude], 11);
        // And display users position
        this.marker = L.marker([position.latitude, position.longitude], {
          icon: new L.Icon.Default(),
        })
          .bindPopup(
            `Your position <br> Latitude: ${position.latitude} <br> Longitude: ${position.longitude}`
          )
          .addTo(this.map);
        this.getNewCoords();
      });
    }
    // just allow the user to click manually
    this.getNewCoords();
  }

  // Listens for input changes
  ngOnChanges(changes: SimpleChanges): void {
    // Update map
    if (this.map && this.myMarkers && this.circleMarker) {
      this.myMarkers.clearLayers();
      this.circleMarker.setRadius(this.mapRadius).addTo(this.myMarkers);
      this.marker.addTo(this.myMarkers);
    }
  }

  private getCurrentPosition(): any {
    return new Observable((observer: Subscriber<any>) => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position: any) => {
          observer.next({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          });
          observer.complete();
        });
      } else {
        observer.error();
      }
    });
  }
}
