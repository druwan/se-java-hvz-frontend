import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Game, gameStates } from '../../../models/backend/Game';
import { GameService } from '../../../services/backend/game.service';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth/auth.service';
import { PlayerService } from '../../../services/backend/player.service';
import { ClientService } from '../../../services/client.service';

interface IGame extends Game {
  isRegistered: false;
}

@Component({
  selector: 'app-game-table',
  templateUrl: './game-table.component.html',
  styleUrls: ['./game-table.component.css'],
})
export class GameTableComponent implements OnInit, OnChanges {
  // Get all games
  games: Game[] = [];
  registered: Number[] = [];
  isLoggedIn = false;
  isAdmin = false;
  @Input() refresh: boolean = false;
  @Output() refreshChange = new EventEmitter<boolean>();

  constructor(
    private gameService: GameService,
    private authService: AuthService,
    private router: Router,
    private playerService: PlayerService,
    private clientService: ClientService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['refresh'].currentValue) {
      this.loadGameList();
    }
  }

  async ngOnInit() {
    this.isLoggedIn = await this.authService.isLoggedIn();
    this.isAdmin = await this.authService.isAdmin();
    const username = await this.authService.getUsername();
    this.gameService.getGames().subscribe({
      next: (value) => {
        this.games = value.payload.sort(this.compareGameStates);
        if (this.isLoggedIn) this.addIsRegisteredToGame(username);
      },
    });
  }

  handleClickDetail(gameId: Number) {
    if (this.isLoggedIn) this.router.navigateByUrl('/game/' + gameId);
  }

  handleDeleteGame(gameId: number, event: any) {
    event.stopPropagation();
    if (window.confirm('Do you really want to delete this game?')) {
      this.gameService.deleteGameById(gameId).subscribe({
        next: (response) => {
          this.gameService.getGames().subscribe({
            next: (value) => {
              this.games = value.payload.sort(this.compareGameStates);
            },
          });
        },
      });
    }
  }

  async loadGameList() {
    const username = await this.authService.getUsername();
    this.gameService.getGames().subscribe({
      next: (value) => {
        this.games = value.payload.sort(this.compareGameStates);
        if (this.isLoggedIn) this.addIsRegisteredToGame(username);
        this.refresh = false;
        this.refreshChange.emit(this.refresh);
      },
    });
  }

  compareGameStates(a: Game, b: Game) {
    function transformStateToNumber(game: Game) {
      switch (game.state) {
        case gameStates.register:
          return 1;
        case gameStates.started:
          return 2;
        case gameStates.completed:
          return 3;
        default:
          return Infinity;
      }
    }

    return transformStateToNumber(a) - transformStateToNumber(b);
  }

  addIsRegisteredToGame(username: String) {
    this.games.forEach((game) => {
      const id = game.gameId;
      this.playerService.getPlayersByGameId(id).subscribe({
        next: (response) => {
          const isRegistered =
            response.payload.findIndex(
              (player) => player.playerName === username
            ) > -1;
          if (isRegistered) {
            game.isRegistered = true;
          }
        },
      });
    });
  }
}
