import {ComponentFixture, TestBed} from '@angular/core/testing';

import {KillFormComponent} from './kill-form.component';

describe('KillFormComponent', () => {
  let component: KillFormComponent;
  let fixture: ComponentFixture<KillFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [KillFormComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KillFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
