import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth/auth.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isLoggedIn: boolean = false;
  username: String = "";
  roles: String[] = [];

  constructor(private authService: AuthService) {
  }

  ngOnInit(): void {
    this.authService.isLoggedIn().then(isLoggedIn => this.isLoggedIn = isLoggedIn);
    this.authService.getRoles().then((roles) => this.roles = roles);
    this.authService.getUsername().then((username) => this.username = username)
  }

  public register() {
    window.history.pushState(window.history.state, '', window.location.origin);
    return this.authService.register();
  }

  public login() {
    window.history.pushState(window.history.state, '', window.location.origin);
    return this.authService.login();
  }

  public logout() {
    return this.authService.logout();
  }
}
