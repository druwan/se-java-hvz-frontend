export interface Chat {
  message: String;
  gameId: Number;
  playerId?: String;
  playerName: String;
  faction: String;
  channel: String;
}
