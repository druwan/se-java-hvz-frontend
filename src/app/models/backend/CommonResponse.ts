export interface CommonResponse<Type> {
  payload: Type;
  success: boolean;
  error: ErrorDetails;
}

export interface ErrorDetails {
  status: number;
  message: string;
}
