import { Player } from './Player';

export interface Game {
  gameId: number;
  title: string;
  state: string;
  // Coordinates
  latitude: number;
  longitude: number;
  // Map size
  radius: number;
  playerLimit: number;
  playerCount?: number;
  isRegistered?: boolean;
  // Timestamp
  timeCreated: Date;
  players: Player[];
}

export const gameStates = {
  register: 'Registration',
  started: 'In Progress',
  completed: 'Completed',
};
