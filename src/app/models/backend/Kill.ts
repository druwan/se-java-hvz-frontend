export interface Kill {
  timeOfDeath?: Date;
  // Ids
  killId?: number;
  victimId?: number;
  killerName?: String;
  victimName?: String;
  gameId: number;
  killerId: number;
  biteCode: string;
  // Coordinates
  latitude: number;
  longitude: number;
}
