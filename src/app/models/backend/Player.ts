export interface Player {
  playerId: number;
  biteCode: string;
  human: boolean;
  patientZero: boolean;
  playerName?: String;
  userName?: string;
  id?: number;
  latitude: 0;
  longitude: 0;
}
