import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable, tap} from 'rxjs';
import {CommonResponse} from '../../models/backend/CommonResponse';
import {Game} from '../../models/backend/Game';
import {Chat} from '../../models/backend/Chat';

interface Response<Type = Game> extends CommonResponse<Type> {
}

export const BACKEND_HOST = "https://se-java-hvz-backend.herokuapp.com"
export const BACKEND_API_PATH = BACKEND_HOST + "/api/v1"; // api/v1/game/...

@Injectable({
  providedIn: 'root'
})
export class GameService {

  currentGame: any = new BehaviorSubject<any | Game>("null");
  GAME_URL = BACKEND_API_PATH + "/game";

  constructor(private http: HttpClient) {
  }

  // GET /game

  // Returns a list of games. Optionally accepts appropriate query parameters.
  getGames(): Observable<CommonResponse<Game[]>> {
    const url = this.GAME_URL;

    const headers = {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT'
    };

    const options = {headers};


    return this.http.get<CommonResponse<Game[]>>(url, options);
  }

  // GET /game/<game_id>
  // Returns a specific game object.
  getGameById(id: string | number): Observable<CommonResponse<Game>> {
    return this.http.get<CommonResponse<Game>>(this.GAME_URL + "/" + id).pipe(tap(next => {
      this.currentGame.next(next.payload);
    }));
  }

  // POST /game
  // Creates a new game. Accepts appropriate parameters in the request body as application/json.
  // Admin only.
  postGame(game: Game): Observable<CommonResponse<Game>> {
    const url = this.GAME_URL;
    const body = game;
    const headers = {
      "Content-Type": "application/json"
      // "Authorization": this.ACCESS_TOKEN,
    }
    const options = {headers};
    return this.http.post<CommonResponse<Game>>(url, body, options);
  }


  // PUT /game/<game_id>
  // Updates a game. Accepts appropriate parameters in the request body as application/json.
  // Admin only.
  updateGameById(id: string | number, newGame: Game): Observable<CommonResponse<Game>> {
    const url = this.GAME_URL + "/" + id;
    const headers = {
      "Content-Type": "application/json",
    }
    const options = {headers};
    return this.http.put<CommonResponse<Game>>(url, newGame, options);

  }

  // DELETE /game/<game_id>
  // Deletes (cascading) a game. Admin only.
  deleteGameById(id: string | number) {
    const url = this.GAME_URL + "/" + id;
    const headers = {
      "Content-Type": "application/json",
    }
    const options = {headers};
    return this.http.delete<Response>(url, options);
  }

  // GET /game/<game_id>/chat
  // Returns a list of chat messages. Optionally accepts appropriate query parameters.
  // The messages returned should take into account the current game state of the player,
  // i.e. a human should recieve chat messages addressed to the ”global” (cross-faction chat)
  // and ”human” chats but not the ”zombie” chat.
  getChatByGameId(id: string | number) {
    const path = "/" + id + "/chat";
    const url = this.GAME_URL + path;
    const headers = {
      "Content-Type": "application/json",
    }
    const options = {headers};
    return this.http.get<Response<Chat[]>>(url, options);
  }

  // POST /game/<game_id>/chat
  // Send a new chat message. Accepts appropriate parameters in the request body as application/json.
  postChatMessage(id: string|number, message: Chat) {

    const path = "/" + id + "/chat";
    const url = this.GAME_URL + path;
    const body = message;
    const headers = {
      "Content-Type": "application/json",
    }
    const options = {headers};
    return this.http.post<Response<Chat>>(url, body);
  }

  getGameObserver(): BehaviorSubject<Game> {
    return this.currentGame;
  }
}
