import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CommonResponse} from '../../models/backend/CommonResponse';
import {Kill} from "../../models/backend/Kill";

export const BACKEND_HOST = "https://se-java-hvz-backend.herokuapp.com"
export const BACKEND_API_PATH = BACKEND_HOST + "/api/v1"; // api/v1/game/...


@Injectable({
  providedIn: 'root'
})

export class KillService {

  GAME_URL = BACKEND_API_PATH + "/kill"
  ACCESS_TOKEN = "";

  constructor(private http: HttpClient) {
  }

  // GET /game/<game_id>/kill
  // Get a list of kills. Optionally accepts appropriate query parameters.
  getkillsByGame(gameId: string | number): Observable<CommonResponse<Kill[]>> {
    const path = "/game/" + gameId + "/kill";
    const url = BACKEND_API_PATH + path;
    return this.http.get<CommonResponse<Kill[]>>(url);
  }

  // GET /game/<game_id>/kill/<kill_id>
  // Returns a specific kill object.
  getkillsByGameIdAndkillId(gameId: string | number, killId: string | number): Observable<CommonResponse<Kill>> {
    const PATH = "/game/" + gameId + "/kill";
    const url = BACKEND_API_PATH + PATH;
    return this.http.get<CommonResponse<Kill>>(url);
  }

  // POST /game/<game_id>/kill
  // Creates a kill object by looking up the victim by the specified bite code. Accepts
  // appropriate parameters in the request body as application/json. Returns 401 Bad
  // Request if the user is not an administrator and the specified bite code is invalid.
  registerkillOnPlayer(gameId: number, kill: Kill): Observable<CommonResponse<Kill>> {
    const PATH = "/game/" + gameId + "/kill";
    const url = BACKEND_API_PATH + PATH;
    const body = kill;
    const headers = {
      "Content-Type": "application/json",
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT',
      'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token'
    };
    const options = {headers};
    return this.http.post<CommonResponse<Kill>>(url, body, options);
  }


  // PUT /game/<game_id>/kill/<kill_id>
  // Updates a kill object. Accepts appropriate parameters in the request body as application/json.
  // Only the killer or an administrator may update a kill object.
  updatekillByGameIdAndkillId(gameId: string | number, killId: string | number, kill: Kill): Observable<CommonResponse<Kill>> {
    const PATH = "/game/" + gameId + "/kill/" + killId;
    const url = BACKEND_API_PATH + PATH;
    return this.http.put<CommonResponse<Kill>>(url, kill);
  }

  // DELETE /game/<game_id>/kill/<kill_id>
  // Delete a kill. Admin only.
  deletekillByGameIdAndkillId(gameId: string | number, killId: string | number): Observable<CommonResponse<Kill>> {
    const PATH = "/game/" + gameId + "/kill/" + killId;
    const url = BACKEND_API_PATH + PATH;
    return this.http.delete<CommonResponse<Kill>>(url);
  }
}
