import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject, tap} from 'rxjs';
import {CommonResponse} from '../../models/backend/CommonResponse';
import {Player} from "../../models/backend/Player";

export const BACKEND_HOST = "https://se-java-hvz-backend.herokuapp.com"
export const BACKEND_API_PATH = BACKEND_HOST + "/api/v1"; // api/v1/game/...

interface IPlayerService {
  getPlayersByGameId(id: string | number): Observable<CommonResponse<Player[]>>;

  getPlayerByGameIdAndPlayerId(gameId: string | number, playerId: string | number): Observable<CommonResponse<Player>>;

  getPlayerIdByGameIdAndPlayerName(gameId: string | number, playerName: string): Observable<CommonResponse<Player>>;

  registerPlayerWithGameId(gameId: string | number, playerName:string): Observable<CommonResponse<Player>>;

  updatePlayerByGameIdAndPlayerID(gameId: string | number, playerId: string | number, player: Player): Observable<CommonResponse<Player>>

  deletePlayerByGameIdAndPlayerId(gameId: string | number, playerId: string | number): Observable<CommonResponse<Player>>;
}


@Injectable({
  providedIn: 'root'
})
export class PlayerService implements IPlayerService {

  player = new Subject();

  constructor(private http: HttpClient) {
  }

  //   GET /game/<game_id>/player
  //   Get a list of players. Player details should be not expose sensitive information such
  //   as the player’s is_patient_zero flag when being accessed by a non-administrator. Optionally accepts appropriate query parameters.
  getPlayersByGameId(id: string | number): Observable<CommonResponse<Player[]>> {
    const PATH = "/game/" + id + "/player";
    const url = BACKEND_API_PATH + PATH;
    return this.http.get<CommonResponse<Player[]>>(url);
  }

  //   GET /game/<game_id>/player
  //   Get a list of players. Player details should be not expose sensitive information such
  //   as the player’s is_patient_zero flag when being accessed by a non-administrator. Optionally accepts appropriate query parameters.
  getPlayersByGameIdAsAdmin(id: string | number): Observable<CommonResponse<Player[]>> {
    const PATH = "/game/" + id + "/player/admin";
    const url = BACKEND_API_PATH + PATH;
    return this.http.get<CommonResponse<Player[]>>(url);
  }

  //   GET /game/<game_id>/player/<player_id>
  //   Returns a specific player object. Player details should be not expose sensitive information such as the player’s is_patient_zero flag when being accessed by a non administrator.
  getPlayerByGameIdAndPlayerId(gameId: string | number, playerId: string | number): Observable<CommonResponse<Player>> {
    const PATH = "/game/" + gameId + "/player/" + playerId;
    const url = BACKEND_API_PATH + PATH;
    return this.http.get<CommonResponse<Player>>(url).pipe((tap((response) => {
      this.player.next(response.payload);
    })));
  }

  getPlayerIdByGameIdAndPlayerName(gameId: String | Number, playerName: String): Observable<CommonResponse<Player>> {
    const PATH = "/game/" + gameId + "/player/name/" + playerName;
    const url = BACKEND_API_PATH + PATH;
    return this.http.get<CommonResponse<Player>>(url).pipe((tap((response) => {
      this.player.next(response.payload);
    })));
  }

  //   POST /game/<game_id>/player
  //   Registers a user for a game. Player objects must be unique per user, per game. Only
  //   administrators may specify appropriate parameters in the request body as application/json
  // – otherwise all parameters assume default values.
  registerPlayerWithGameId(gameId: string | number, playerName:String): Observable<CommonResponse<Player>> {
    const PATH = "/game/" + gameId + "/player";
    const url = BACKEND_API_PATH + PATH;
    return this.http.post<CommonResponse<Player>>(url, {playerName}).pipe((tap((response) => {
      this.player.next(response.payload);
    })));
  }

  //   PUT /game/<game_id>/player/<player_id>
  //   Updates a player object. Accepts appropriate parameters in the request body as
  //   application/json. Admin only.
  updatePlayerByGameIdAndPlayerID(gameId: string | number, playerId: string | number, player: Player): Observable<CommonResponse<Player>> {
    const PATH = "/game/" + gameId + "/player/" + playerId;
    const url = BACKEND_API_PATH + PATH;
    const headers = {
      "Access-Control-Allow-Origin": "*"
    };
    const options = {headers};
    return this.http.put<CommonResponse<Player>>(url, player, options).pipe((tap((response) => {
      this.player.next(response.payload);
    })));
  }

  //   DELETE /game/<game_id>/player/<player_id>
  //   Deletes (cascading) a player. Admin only.
  deletePlayerByGameIdAndPlayerId(gameId: string | number, playerId: string | number): Observable<CommonResponse<Player>> {
    const PATH = "/game/" + gameId + "/player/" + playerId;
    const url = BACKEND_API_PATH + PATH;
    return this.http.delete<CommonResponse<Player>>(url).pipe((tap((response) => {
      this.player.next(null);
    })));
  }


}
