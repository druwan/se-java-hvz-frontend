import { Injectable } from '@angular/core';
import { Client, Message, over, Subscription } from 'stompjs';
import * as SockJS from 'sockjs-client';
import { environment } from '../../environments/environment';
import { Game, gameStates } from '../models/backend/Game';
import { Subject } from 'rxjs';
import { Kill } from '../models/backend/Kill';
import { Player } from '../models/backend/Player';
import { GameService } from './backend/game.service';
import { PlayerService } from './backend/player.service';
import { KillService } from './backend/kill.service';

@Injectable({
  providedIn: 'root',
})
/**
 * CLient service is used to manage a session
 */
export class ClientService {
  client: Promise<Client>;
  subscriptions: Subscription[] = [];
  playerSub?: Subscription;
  gameList?: Game[];
  game?: Game;

  constructor(
    private gameService: GameService,
    private playerService: PlayerService,
    private kill: KillService
  ) {
    this.client = this.connect();
  }

  // subscribe to /game/gameId
  public async subscribeToGame(id: String) {
    const subject = new Subject<Game>();
    this.gameService.getGameById(id + '').subscribe({
      next: (response) => {
        const destination = '/topic/game/' + id;
        const game = response.payload;
        subject.next(game);
        this.game = game;
        this.subscribeTo(destination, (res: Message) => {
          const body = res.body;
          const game: Game = JSON.parse(body);
          console.log('game has updated: ' + game);
          this.game = game;
          subject.next(game);
          if (game.state === 'Completed') {
            console.log('printing winners');
            const winner = this.playerService
              .getPlayersByGameId(id + '')
              .subscribe({
                next: (response) => {
                  const humans = response.payload.filter(
                    (player) => player.human
                  );
                  console.log(humans);
                  humans.forEach((human) =>
                    this.alert(`Winner is: ${human.playerName}`)
                  );
                },
              });
          }
          this.alert('Game state has updated to: ' + game.state);
        });
      },
    });
    return subject;
  }

  public subscribeToPlayerList(gameId: number): Subject<Player[]> {
    console.log('subscribe to player');
    const subject = new Subject<Player[]>();
    const destination = '/topic/game/' + gameId + '/player';
    const onPlayerListUpdated = (message: Message) => {
      const players: Player[] = JSON.parse(message.body).sort();
      console.log('Updating playerList');
      console.log(players);
      subject.next(players);
    };
    this.subscribeTo(destination, onPlayerListUpdated);
    return subject;
  }

  // subscribe to /game/gameId/kill
  public subscribeToKillsByGame(id: String): Subject<Kill[]> {
    console.log('subscribe to kill');
    const subject = new Subject<Kill[]>();
    const destination = '/topic/game/' + id + '/kill';
    this.subscribeTo(destination, (res: Message) => {
      const body = res.body;
      const object: Kill[] = JSON.parse(body);
      console.log('new player has been killed: ' + object);
      subject.next(object);
    });
    return subject;
  }

  /***
   * Receives new player location object to update the map
   * @param id - id of game
   * @returns {subject<Player>} - player object that contains latitude and longitude
   */
  public async subscribeToPlayerLocationByGame(id: String) {
    console.log('subscribe to player location');
    const subject = new Subject<Player>();
    const destination = '/topic/game/' + id + '/player' + '/location';
    const onNewPlayerMoved = (message: Message) => {
      const player: Player = JSON.parse(message.body);
      subject.next(player);
    };
    this.subscribeTo(destination, onNewPlayerMoved);
    return subject;
  }

  public async broadcastPlayerPositionByGame(id: String, player: Player) {
    const destination = '/topic/game/' + id + '/player' + '/location';
    this.broadcastMessage(destination, player);
  }

  public async broadcastMessage(destination: string, body: any) {
    const client = await this.client;
    client.send(destination, {}, JSON.stringify(body));
  }

  public async subscribeTo(destination: string, callback?: any) {
    const client = await this.client;
    const sub = client.subscribe(destination, callback);
    this.subscriptions.push(sub);
  }

  public unsubscribeAll() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
    this.subscriptions = [];
  }

  public subscribeToPlayer(gameId: number, playerId: number) {
    console.log('subscribe to player');
    const subject = new Subject<Player>();
    const destination = '/topic/game/' + gameId + '/player/' + playerId;
    const onPlayerUpdated = (message: Message) => {
      const player: Player = JSON.parse(message.body);
      if (player.human) {
        this.alert('You are a human');
      } else if (!player.human) {
        if (player.patientZero) {
          this.alert('You are patient zero');
        } else {
          this.alert('You are a zombie');
        }
      }
      subject.next(player);
    };
    this.subscribeTo(destination, onPlayerUpdated);
    return subject;
  }

  public subscribeToNewPlayer(gameId: number): Subject<Player> {
    console.log('subscribe to player');
    const subject = new Subject<Player>();
    const destination = '/topic/game/' + gameId + '/player';
    const onNewPlayer = (message: Message) => {
      const player: Player = JSON.parse(message.body);
      subject.next(player);
    };
    this.subscribeTo(destination, onNewPlayer);
    return subject;
  }

  public sendNewPlayer(gameId: number, player: Player) {
    const destination = '/topic/game/' + gameId + '/player';
    this.send(destination, player);
  }

  public async send(destination: string, body: any) {
    const client = await this.client;
    client.send(destination, {}, JSON.stringify(body));
  }

  unsubscribePlayer() {
    this.playerSub?.unsubscribe();
  }

  public alert(message: String) {
    console.log(message);
    if (
      this.game?.state === gameStates.started ||
      this.game?.state === gameStates.completed
    ) {
      setTimeout(() => alert(message));
    }
  }

  private async connect() {
    const client = over(new SockJS(environment.BACKEND_SOCKET_URL));
    if (!client.connected) {
      return new Promise<Client>((resolve, reject) => {
        client.connect(
          {},
          () => {
            console.log('client has connected');
            resolve(client);
          },
          (error) => {
            console.log(error);
            reject(error);
          }
        );
      });
    } else {
      return client;
    }
  }
}
