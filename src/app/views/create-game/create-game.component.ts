import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Game } from 'src/app/models/backend/Game';
import { GameService } from 'src/app/services/backend/game.service';
import { faker } from '@faker-js/faker';

@Component({
  selector: 'app-create-game',
  templateUrl: './create-game.component.html',
  styleUrls: ['./create-game.component.css'],
})
export class CreateGameComponent implements OnInit {
  @Output() updateGameRadius = new EventEmitter();
  @ViewChild('createGame') gameForm: NgForm | undefined;

  game: Game | any;
  map: any;

  mapLong: number = 0;
  mapLat: number = 0;
  window: any;
  // Map properties
  gameRadius: any = 100;
  maxRadius = 3000;
  minRadius = 100;
  isLoading = false;

  constructor(private router: Router, private gameService: GameService) {}

  ngOnInit(): void {
    setTimeout(() => {
      const defaultValues = {
        title: faker.name.firstName(),
        playerLimit: 10,
        radius: this.gameRadius,
      };
      this.gameForm?.setValue(defaultValues);
    });
  }

  public onSliderChange($event: any) {
    this.gameRadius = parseInt($event);
  }

  newCoordinates(coord: any) {
    this.mapLat = coord.Latitude;
    this.mapLong = coord.Longitude;
  }

  public onSubmit(gameForm: NgForm): void {
    this.isLoading = true;
    // Get data from input form
    const formObj = gameForm.value;
    // Push onto DB

    const gameTitle = formObj.title;
    const playerLimit = formObj.playerLimit;
    const latitude = this.mapLat;
    const longitude = this.mapLong;

    this.game = {
      title: gameTitle,
      state: 'Registration',
      latitude: latitude,
      longitude: longitude,
      playerLimit: playerLimit,
      radius: this.gameRadius,
    };

    this.gameService.postGame(this.game).subscribe({
      next: (response) => {
        const newGame = response.payload;
        // Redirect player to landing page
        this.goBack();
      },
      error(msg) {
        console.log(`Error: ${JSON.stringify(msg)}`);
        alert('Error could not create game');
      },
    });
  }

  goBack(): void {
    // need to add a reload so we don't have to refresh the page manually?
    this.router.navigateByUrl('');
  }

  onSliderInput(event: any) {
    const input = event.target;
    this.gameRadius = input.value;
  }
}
