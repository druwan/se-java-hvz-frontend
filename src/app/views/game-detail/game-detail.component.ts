import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GameService } from 'src/app/services/backend/game.service';
import * as L from 'leaflet';
import { LeafletMouseEvent } from 'leaflet';
import { environment } from 'src/environments/environment';
import { NgForm } from '@angular/forms';
import { PlayerService } from 'src/app/services/backend/player.service';
import { Player } from 'src/app/models/backend/Player';
import { KillService } from 'src/app/services/backend/kill.service';
import { Kill } from 'src/app/models/backend/Kill';
import { AuthService } from '../../services/auth/auth.service';
import { Game, gameStates } from '../../models/backend/Game';
import { ClientService } from '../../services/client.service';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.css'],
})
export class GameDetailComponent implements OnInit, OnDestroy {
  game: any | Game;
  gameMap: any | L.Map;
  gameArea?: L.Circle;
  playerLayer: L.Layer | any;
  player: any | Player;
  playerInfo: any;
  kill: any;
  killsLayer?: L.LayerGroup;
  playerName: String = '';
  playersInGame: Player[] = [];
  position = {
    latitude: 0,
    longitude: 0,
  };
  victimId: any;
  isAdmin = false;
  gameId: number = Number(this.route.snapshot.paramMap.get('id'));
  isUpdatingPlayer = false;
  isLoadingGame = false;
  geoWatchId?: number;
  private readonly defaultZoom = 14;
  private fetchPlayerListId?: NodeJS.Timeout;

  constructor(
    private gameService: GameService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private playerService: PlayerService,
    private killService: KillService,
    private clientService: ClientService
  ) {}

  get isRegistering() {
    return this.checkGameState(gameStates.register);
  }

  get isProgressing() {
    return this.checkGameState(gameStates.started);
  }

  async ngOnInit() {
    // Get username from keycloak
    this.playerName = await this.authService.getUsername();
    this.isAdmin = await this.authService.isAdmin();

    this.getGameFromSelectedUrl().subscribe({
      next: (response) => {
        const game = response.payload;
        this.game = game;
        console.log(game);
        const latitude = this.game.latitude;
        const longitude = this.game.longitude;
        const radius = this.game.radius;
        this.initMap(latitude, longitude, radius);
        // get kills
        this.clientService
          .subscribeToKillsByGame(this.game.gameId)
          .subscribe((kills: Kill[]) => {
            this.addKillsToMap(kills);
          });
        this.clientService.subscribeToGame(this.game.gameId).then((subject) =>
          subject.subscribe((next: Game) => {
            this.game = next;
            if (this.game.players) {
              this.playersInGame = this.game.players;
            }
            // Fetch players manually
            else {
              this.getPlayerList().subscribe({
                next: (response) => {
                  this.playersInGame = response.payload;
                },
              });
            }
          })
        );

        // add kill markers to map
        this.killService.getkillsByGame(this.gameId).subscribe({
          next: (value) => {
            // add kills to map
            value.payload.forEach((kill: Kill) => {
              this.addKillToMap(kill);
            });
          },
        });

        this.clientService
          .subscribeToPlayerList(this.gameId)
          .subscribe((players) => {
            this.playersInGame = players;
          });
        this.getPlayerList().subscribe({
          next: (value) => {
            this.playersInGame = value.payload;
            // check if player is registered to the game
            const player = this.getPlayerByUsername(this.playerName);
            // player is registered to game
            if (player) {
              this.player = player;
              // listen to player changes
              this.clientService
                .subscribeToPlayer(this.gameId, player.playerId)
                .subscribe((newPlayer: Player) => {
                  const player = this.playersInGame.find(
                    (player) => player.playerId === newPlayer.playerId
                  );
                  if (player) {
                    player.human = newPlayer.human;
                    this.player = player;
                  }
                });
            }
            if (this.playerIsRegistered() && this.isZombie()) {
              this.getPlayerLocation();
            }
          },
        });
      },
    });
  }

  ngOnDestroy() {
    this.clientService.unsubscribeAll();
    if (this.fetchPlayerListId) {
      clearInterval(this.fetchPlayerListId);
    }
  }

  getGameFromSelectedUrl() {
    const gameId = Number(this.route.snapshot.paramMap.get('id'));
    return this.gameService.getGameById(gameId);
  }

  // get coordinates from kill objects

  // Create map
  initMap(latitude: number, longitude: number, radius: number): void {
    // Create a new map
    this.gameMap = L.map('gameMap').setView([latitude, longitude], 14);
    L.tileLayer(
      'https://api.maptiler.com/maps/toner/{z}/{x}/{y}.png?key={accessToken}',
      {
        attribution:
          '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>',
        accessToken: environment.MAPTILER_KEY,
      }
    ).addTo(this.gameMap);
    // Add layers
    this.killsLayer = new L.LayerGroup();
    this.killsLayer.addTo(this.gameMap);
    // Mark game area
    this.BaseMarker({ latitude, longitude }).addTo(this.gameMap);
    this.gameArea = this.GameArea(latitude, longitude, radius).addTo(
      this.gameMap
    );
  }

  // and add that location to the map as a zombie
  addKillsToMap(kills?: Kill[]): void {
    this.killsLayer?.clearLayers();
    kills?.forEach((kill) => this.addKillToMap(kill));
  }

  ZombieMarker(position: any) {
    const redIcon = new L.Icon({
      iconUrl:
        'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png',
      shadowUrl:
        'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41],
    });
    return L.marker([position.latitude, position.longitude], {
      icon: redIcon,
    })
      .bindPopup(
        `Your position <br> Latitude: ${position.latitude} <br> Longitude: ${position.longitude}`
      )
      .bindTooltip(`Zombie`);
  }

  HumanMarker(position: any) {
    const blueIcon = new L.Icon({
      iconUrl:
        'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-blue.png',
      shadowUrl:
        'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41],
    });
    return L.marker([position.latitude, position.longitude], {
      icon: blueIcon,
    })
      .bindPopup(
        `Your position <br> Latitude: ${position.latitude} <br> Longitude: ${position.longitude}`
      )
      .bindTooltip(`Human`);
  }

  BaseMarker(game: any) {
    const greyIcon = new L.Icon({
      iconUrl:
        'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-grey.png',
      shadowUrl:
        'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41],
    });
    return L.marker([game.latitude, game.longitude], {
      icon: greyIcon,
    })
      .bindPopup(
        `Game Area of ${this.game?.title}, with a radius of ${this.game?.radius} meters.`
      )
      .bindTooltip(`Game Marker`);
  }

  TombstoneMarker(kill: Kill) {
    let zombieIcon = L.icon({
      iconUrl: '../assets/map-marker/Tombstone.svg',
      iconSize: [25, 30],
      popupAnchor: [5, -10],
    });
    const { latitude, longitude, victimId } = kill;
    // find victim name
    const victim = this.playersInGame.find(
      (player) => player.playerId === victimId
    );
    return L.marker([latitude, longitude], {
      icon: zombieIcon,
    })
      .bindPopup(
        `player ${kill.killerName} killed ${
          kill.victimName
        } ${kill.timeOfDeath?.toString()}`
      )
      .bindTooltip(`Zombie`);
  }

  handleKill($event: Kill) {
    // add kill to map
    this.addKillToMap($event);
  }

  async getPlayerLocation() {
    console.log('Try geolocation');
    if (navigator.geolocation) {
      try {
        const position = await this.getCurrentGeoPosition();
        this.position = position;
        this.addPlayerToMap(position);
        return position;
      } catch (error) {
        console.log('Geo location is disabled');
        this.addClickHandlerToMap();
      }
    }
  }

  joinGame(): void {
    // Only need to send playerName to the post request
    this.playerService
      .registerPlayerWithGameId(this.gameId, this.playerName)
      .subscribe({
        next: (response) => {
          if (!response.success) {
            throw new Error('Unable to register to game');
          }
          // Update player list
          console.log(response.payload);
          const registeredPlayer: Player = response.payload;
          this.player = registeredPlayer;
          this.clientService
            .subscribeToPlayer(this.gameId, registeredPlayer.playerId)
            .subscribe((newPlayer: Player) => {
              if (newPlayer) {
                // check if player is in list
                // if player is not in iist add player
                // update player in this component
                const player = this.playersInGame.find(
                  (player) => player.playerId === newPlayer.playerId
                );
                if (player) {
                  player.human = newPlayer.human;
                  this.player = player;
                }
              }
            });
        },
        error: () => {
          this.alert('Failed to register to game');
        },
      });
  }

  alert(msg: String) {
    setTimeout(() => alert(msg));
  }

  showPlayerInformation() {
    this.playerService
      .getPlayerIdByGameIdAndPlayerName(this.gameId, this.playerName)
      .subscribe({
        next: (value) => {
          this.playerInfo = value.payload;
        },
      });
  }

  updateGameFromForm(updateForm: NgForm) {
    // If the value in the form was not empty -> Update property. Else, keep the previous
    if (updateForm.value.updatedTitle != '') {
      this.game.title = updateForm.value.updatedTitle;
    } else {
      // TODO: Makes no sense.
      this.game.title = this.game.title;
    }

    if (updateForm.value.updatedState != '') {
      this.game.state = updateForm.value.updatedState;
    } else {
      // TODO: Makes no sense.
      this.game.state = this.game.state;
    }

    this.gameService.updateGameById(this.gameId, this.game).subscribe({
      next: (value) => {
        value.payload = this.game;
      },
    });
  }

  flipState(playerId: number): void {
    this.isUpdatingPlayer = true;
    // Get new player object
    // Find player in playerList
    const player = this.playersInGame.find(
      (player) => player.playerId === playerId
    );
    if (player) {
      player.human = !player.human;
      this.player.human = player.human;
      // Update server
      this.playerService
        .updatePlayerByGameIdAndPlayerID(this.gameId, playerId, player)
        .subscribe({
          next: (value) => {
            // check if player human property is true
            if (player.human === value.payload.human)
              console.log('update successful');
          },
        });
    }
  }

  goBack(): void {
    this.router.navigateByUrl('');
  }

  leaveGame() {
    this.playerService
      .deletePlayerByGameIdAndPlayerId(this.gameId, this.player.playerId)
      .subscribe({
        next: (response) => {
          if (response.error) {
            throw new Error('Unable to register to game');
          }
          // Update player list
          console.log(response.payload);
          const removePlayer: Player = response.payload;
          this.playersInGame = this.playersInGame.filter(
            (player) => player.playerId !== removePlayer.playerId
          );
          this.player = undefined;
          this.clientService.unsubscribePlayer();
        },
        error: () => {
          alert('Failed to leave game');
          // remove player from playerList
        },
      });
  }

  playerIsRegistered(): boolean {
    return (
      this.playersInGame.findIndex(
        (player) => this.player?.playerId === player?.playerId
      ) > -1
    );
  }

  flyToCurrentPosition() {
    this.gameMap.flyTo(
      [this.position.latitude, this.position.longitude],
      this.defaultZoom,
      { animate: false }
    );
  }

  geoLocationEnabled() {
    return 'geolocation' in navigator;
  }

  /***
   * checks whether player is human or zombie
   * returns {boolean}
   */
  isZombie(): boolean {
    return !this.player?.human;
  }

  startGame() {
    this.setGameState('In Progress');
  }

  resetGame() {
    this.setGameState('Registration');
  }

  setGameState(state: String) {
    // Copy game object
    const game = JSON.parse(JSON.stringify(this.game));
    game.state = state;
    this.isLoadingGame = true;
    return this.gameService.updateGameById(game.gameId, game).subscribe({
      next: (response) => {
        if (response.success) {
          this.game = response.payload;
          console.log(this.game);
        } else {
          this.alert(response.error.message);
        }
      },
      error: () => {
        this.alert('failed to set game');
      },
      complete: () => {
        this.isLoadingGame = false;
      },
    });
  }

  checkGameState(state: String) {
    return this.game.state === state;
  }

  /**
   * Enable listen to geolocation
   */
  watchGeolocation() {
    this.geoWatchId = navigator.geolocation.watchPosition(
      (position: GeolocationPosition) => {
        console.log('current position');
        this.position = position.coords;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  unWatchGeolocation() {
    if (this.geoWatchId) {
      navigator.geolocation.clearWatch(this.geoWatchId);
    }
  }

  getPlayerList() {
    if (this.isAdmin) {
      return this.playerService.getPlayersByGameIdAsAdmin(this.gameId);
    } else {
      return this.playerService.getPlayersByGameId(this.gameId);
    }
  }

  private GameArea(
    latitude: number,
    longitude: number,
    radius: number
  ): L.Circle {
    return L.circle([latitude, longitude], {
      stroke: false,
      color: '#B30808',
      fillOpacity: 0.55,
      radius: radius,
    });
  }

  private addPlayerToMap(position: any) {
    this.HumanMarker(position).addTo(this.gameMap);
  }

  private addKillToMap(kill: Kill) {
    if (this.killsLayer) {
      this.TombstoneMarker(kill).addTo(this.killsLayer);
    }
  }

  private setPosition(lat: number, long: number) {
    this.position = { latitude: lat, longitude: long };
  }

  private addClickHandlerToMap() {
    console.log('add click handler');
    if (this.gameArea) {
      return this.gameArea.addEventListener('click', this.handleClickMap);
    } else if (this.gameMap) {
      return this.gameMap.addEventListener('click', this.handleClickMap);
    }
  }

  private handleClickMap = (e: LeafletMouseEvent) => {
    const latitude: number = e.latlng.lat;
    const longitude: number = e.latlng.lng;
    this.setPosition(latitude, longitude);
    let marker: L.Marker;
    if (this.player) {
      if (this.player.human) {
        marker = this.HumanMarker({ latitude, longitude });
      } else marker = this.ZombieMarker({ latitude, longitude });

      if (this.playerLayer) {
        this.gameMap.removeLayer(this.playerLayer);
      }
      this.playerLayer = L.layerGroup();
      marker.addTo(this.playerLayer);
      this.gameMap.addLayer(this.playerLayer);
    }
  };

  private getCurrentGeoPosition(): Promise<any> {
    return new Promise((resolve, reject) => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
          resolve(position.coords);
        }, reject);
      }
    });
  }

  private getPlayerByUsername(username: String) {
    const player = this.playersInGame.find(
      (player) => player.playerName === username
    );
    return player;
  }
}
