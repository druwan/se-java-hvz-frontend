import { Component, OnInit } from '@angular/core';
import { Game } from 'src/app/models/backend/Game';
import { GameService } from 'src/app/services/backend/game.service';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css'],
})
export class LandingPageComponent implements OnInit {
  // Get all games
  games: Game[] = [];
  roles: String[] = [];
  refreshGameTable: boolean = false;

  constructor(
    public gameService: GameService,
    private router: Router,
    private authService: AuthService
  ) {}

  async ngOnInit() {
    this.gameService.getGames().subscribe({
      next: (value) => {
        this.games = value.payload;
      },
    });
    this.roles = await this.authService.getRoles();
  }

  onClickNewGame(): void {
    this.router.navigateByUrl('newgame');
  }

  onClickHome(): void {
    this.router.navigateByUrl('');
  }

  isAdmin() {
    return this.roles.find((role) => role === 'admin');
  }

  onRefresh() {
    this.refreshGameTable = true;
  }
}
